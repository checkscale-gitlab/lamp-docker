CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `tipo` varchar(50) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE IF NOT EXISTS `area` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `pantalla` varchar(50) NOT NULL,
  `capacidad` varchar(50) NOT NULL,
  `id_categoria` int,
  CONSTRAINT `FK_area_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria`(`id`),
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


 CREATE TABLE IF NOT EXISTS `provincia` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


 CREATE TABLE IF NOT EXISTS `localidad` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


 CREATE TABLE IF NOT EXISTS `departamento` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE IF NOT EXISTS `cultura` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_categoria` int ,
   CONSTRAINT `FK_espacioCultural_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria`(`id`),
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `piso` varchar(10) NOT NULL,
  `año_inicio` int(11) NOT NULL,
  `informacion` varchar(100) NOT NULL,
  `id_provincia` int NOT NULL,
  CONSTRAINT `FK_espacioCultural_provincia` FOREIGN KEY (`id_provincia`) REFERENCES `provincia`(`id`),
  `id_localidad` int NOT NULL,
  CONSTRAINT `FK_espacioCultural_localidad` FOREIGN KEY (`id_localidad`) REFERENCES `localidad`(`id`),
  `id_departamento` int NOT NULL,
  CONSTRAINT `FK_espacioCultural_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamento`(`id`),
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE IF NOT EXISTS `contacto` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cod_area` varchar(5) NULL,
  `telefono` varchar(5) NULL,
  `email` varchar(200)NOT NULL,
  `web` varchar(200)NULL,
  `id_esp_cultural` INT NOT NULL,
  CONSTRAINT `FK_contacto_espacioCultural` FOREIGN KEY (`id_esp_cultural`) REFERENCES `espacio_cultural`(`id`),
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

