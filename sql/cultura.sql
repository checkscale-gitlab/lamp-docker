-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql
-- Tiempo de generación: 22-03-2022 a las 20:30:08
-- Versión del servidor: 5.7.37
-- Versión de PHP: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `MapaCultural`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cultura`
--

CREATE TABLE `cultura` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(100) DEFAULT 's/d',
  `piso` varchar(10) DEFAULT 's/d',
  `Pantallas` int(11) DEFAULT NULL,
  `Capacidad` int(11) DEFAULT NULL,
  `ao_inicio` int(11) DEFAULT '0',
  `f_inauguracion` date DEFAULT NULL,
  `informacion` varchar(100) DEFAULT NULL,
  `id_departamento` int(11) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cultura`
--

INSERT INTO `cultura` (`id`, `id_categoria`, `nombre`, `direccion`, `piso`, `Pantallas`, `Capacidad`, `ao_inicio`, `f_inauguracion`, `informacion`, `id_departamento`, `reg_date`, `reg_update`) VALUES
(31, 4, 'Cine Complejo Crculo', 'Andres Pazos 339', 's/d', 2, 300, 2018, NULL, NULL, 30, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(32, 4, 'Starlight', 'Calle 12 De Abril 287', 's/d', 2, 154, 2018, NULL, NULL, 30, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(33, 2, 'Altos Del Gualeguay', 'Rocamora 33', 's/d', 1, 198, 2018, NULL, NULL, 30, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(34, 4, 'Barbarita Cruz', 'Belgrano 547', 's/d', 1, 120, 2018, NULL, NULL, 38, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(35, 4, 'Cine Auditorium', 'Independencia Esq. Italia S/N', 's/d', 1, 201, 2018, NULL, NULL, 38, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(36, 3, 'Centro Cultural Doblas', 'Cordoba 670', 's/d', 1, 114, 2020, '2014-10-18', NULL, 42, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(37, 4, 'Milenium', 'Escalante 270', 'Piso 1', 1, 352, 2015, '1971-04-23', 'TURISMO', 42, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(38, 4, 'Cinema', 'La Plata Y 19 De Febrero', 's/d', 2, 216, 2018, NULL, NULL, 46, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(39, 4, 'Cine Teatro Municipal San Martin', 'Av. San Martin 1474', 's/d', 1, 520, 2018, NULL, NULL, 54, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(40, 1, 'Cine Panoramic Grand', 'Paraguay 372', 's/d', 1, 40, 2018, NULL, NULL, 54, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(41, 1, 'Del Conocimiento', 'Ruta 12 Y Acceso Oeste (Av. Ulises Lopez)', 's/d', 1, 546, 2018, NULL, NULL, 54, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(42, 1, 'Centro Cultural 13 De Febrero', '20 De Febrero Esq. Gorriti', 's/d', 1, 252, 2018, '2010-05-30', NULL, 66, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(43, 4, 'El Teatrino', 'Aniceto Latorre 1211', 's/d', 1, 120, 2018, NULL, NULL, 66, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(44, 1, 'Cinemark', 'Av. Monseor Tavella 4400', 's/d', 5, 897, 2018, NULL, NULL, 66, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(45, 4, 'Fundacin Salta', 'General Guemes 434', 'Piso 9', 1, 145, 2018, NULL, NULL, 66, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(46, 3, 'Espacio San Juan', 'Av. Jose Ignacio De La Roza Oeste 806', 's/d', 5, 849, 2018, '1855-01-25', NULL, 70, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(47, 4, 'Cinestar', 'Av. Del Sol 1120', 's/d', 1, 100, 2018, '1954-03-14', NULL, 74, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(48, 4, 'Cine Rio Gallegos', 'Av. Nestor Kirchner 1142', 's/d', 3, 290, 2018, NULL, NULL, 78, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(49, 2, 'Cine Municipal', 'Av. San Martin 514', 'Piso 10', 1, 80, 2018, '2013-11-30', NULL, 78, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(50, 1, 'Centro Cultural 13 De Febrero', '20 De Febrero Esq. Gorriti', 's/d', 1, 252, 2018, '2010-05-30', NULL, 66, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(51, 4, 'El Teatrino', 'Aniceto Latorre 1211', 's/d', 1, 120, 2018, NULL, NULL, 66, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(52, 1, 'Cinemark', 'Av. Monseor Tavella 4400', 's/d', 5, 897, 2018, NULL, NULL, 66, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(53, 4, 'Fundacin Salta', 'General Guemes 434', 's/d', 1, 145, 2018, NULL, NULL, 66, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(54, 3, 'Espacio San Juan', 'Av. Jose Ignacio De La Roza Oeste 806', 's/d', 5, 849, 2018, '1855-01-25', NULL, 70, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(55, 4, 'Cinestar', 'Av. Del Sol 1120', 's/d', 1, 100, 2018, '1954-03-14', NULL, 74, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(56, 4, 'Cine Rio Gallegos', 'Av. Nestor Kirchner 1142', 's/d', 3, 290, 2018, NULL, NULL, 78, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(57, 2, 'Cine Municipal', 'Av. San Martin 514', 's/d', 1, 80, 2018, '2013-11-30', NULL, 78, '2022-03-22 10:44:44', '2022-03-22 10:44:44'),
(70, 1, 'Deborah Jones De Williams', 'Reg. Inf. Mec. 25 Esquina 20 De Junio', 's/d', 1, 80, 2015, '2012-10-21', NULL, 26, '2022-03-22 10:48:47', '2022-03-22 10:48:47'),
(71, 3, 'Fantasio', 'Salta 1059', 's/d', NULL, NULL, 2013, NULL, NULL, 18, '2022-03-22 10:48:47', '2022-03-22 10:48:47'),
(72, 2, 'Cinema Gualeguaychú', '25 De Mayo 1383', 'Piso 1', NULL, NULL, 2011, NULL, NULL, 30, '2022-03-22 10:48:47', '2022-03-22 10:48:47'),
(73, 3, 'Salón Teatro Municipal', '25 De Mayo 943', 's/d', 1, 180, 0, NULL, NULL, 30, '2022-03-22 10:48:47', '2022-03-22 10:48:47'),
(74, 4, 'Cine Gran Libertad', '3 De Febrero 3060', 's/d', 0, 400, 0, '2015-12-23', NULL, 30, '2022-03-22 10:48:47', '2022-03-22 10:48:47'),
(75, 4, 'Los Cines De La Costa', 'Av. Sarmiento 2600', 's/d', 5, 820, 0, NULL, NULL, 22, '2022-03-22 10:48:47', '2022-03-22 10:48:47');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cultura`
--
ALTER TABLE `cultura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_espacioCultural_categoria` (`id_categoria`),
  ADD KEY `FK_espacioCultural_departamento` (`id_departamento`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cultura`
--
ALTER TABLE `cultura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cultura`
--
ALTER TABLE `cultura`
  ADD CONSTRAINT `FK_espacioCultural_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`),
  ADD CONSTRAINT `FK_espacioCultural_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
