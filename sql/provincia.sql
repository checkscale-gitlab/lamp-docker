-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql
-- Tiempo de generación: 22-03-2022 a las 20:39:37
-- Versión del servidor: 5.7.37
-- Versión de PHP: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `MapaCultural`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id`, `nombre`, `reg_date`, `reg_update`) VALUES
(1, 'Misiones', '2022-03-19 08:21:46', '2022-03-19 08:21:46'),
(2, 'San Luis', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(3, 'San Juan', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(4, 'Entre Rios', '2022-03-19 08:21:47', '2022-03-19 19:39:44'),
(5, 'Santa Cruz', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(6, 'Rio Negro', '2022-03-19 08:21:47', '2022-03-19 19:39:44'),
(7, 'Chubut', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(8, 'Cordoba', '2022-03-19 08:21:47', '2022-03-19 19:39:44'),
(9, 'Mendoza', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(10, 'La Rioja', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(11, 'Catamarca', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(12, 'La Pampa', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(13, 'Santiago del Estero', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(14, 'Corrientes', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(15, 'Santa Fe', '2022-03-19 08:21:47', '2022-03-19 08:21:47'),
(16, 'Tucuman', '2022-03-19 08:21:47', '2022-03-19 19:39:44'),
(17, 'Neuquen', '2022-03-19 08:21:47', '2022-03-19 19:39:46'),
(18, 'Salta', '2022-03-19 08:21:48', '2022-03-19 08:21:48'),
(19, 'Chaco', '2022-03-19 08:21:48', '2022-03-19 08:21:48'),
(20, 'Formosa', '2022-03-19 08:21:48', '2022-03-19 08:21:48'),
(21, 'Jujuy', '2022-03-19 08:21:48', '2022-03-19 08:21:48'),
(22, 'Buenos Aires', '2022-03-19 08:21:48', '2022-03-19 08:21:48'),
(23, 'Tierra del Fuego', '2022-03-19 08:21:48', '2022-03-19 08:21:48'),
(24, 'Ciudad Autonoma de Buenos Aires', '2022-03-19 08:21:49', '2022-03-20 06:45:03');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
