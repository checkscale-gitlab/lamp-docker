-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql
-- Tiempo de generación: 22-03-2022 a las 20:28:39
-- Versión del servidor: 5.7.37
-- Versión de PHP: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `MapaCultural`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL,
  `cod_area` varchar(20) DEFAULT 's/d',
  `telefono` varchar(20) DEFAULT 's/d',
  `email` varchar(200) DEFAULT 's/d',
  `web` varchar(200) DEFAULT 's/d',
  `id_esp_cultural` int(11) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`id`, `cod_area`, `telefono`, `email`, `web`, `id_esp_cultural`, `reg_date`, `reg_update`) VALUES
(1, '3764', '191010', 'info@imaxdelconocimiento.com', 'https://www.parquedelconocimiento.com/', 54, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(2, '3874', '496276', 'cine13defebrero2013@outlook.com', 'https://www.facebook.com/Centro-Cultural-13-de-Febrero-Cine-teatro-962533320483179/', 66, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(3, '387', '4228904', 'industriaculturalsrl@gmail.com', 'http://www.elteatrino.com/', 66, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(4, 's/d', '57771000', 'lventura@cinemark.com.ar', 'https://www.cinemarkhoyts.com.ar', 66, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(5, '387', '4320481', 'correofundacionsalta@gmail.com', 'http://www.distritoteatralsalta.com/espacio/teatro-de-la-fundacion-salta/', 66, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(6, '264', '4238611', 'cpohl@cpmcines.com', 'http://www.espaciosanjuan.com/', 70, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(7, '2664', '475660', 'campos_horacio@yahoo.com.ar', 'https://www.facebook.com/CineStarMerlo/', 74, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(8, '2664', '403539', 'elartenlaescuela@gmail.com', 'https://www.ciudaddesanluis.gov.ar/', 74, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(9, '2966', '423753', 'socin@hotmail.com', 'http://www.cineriogallegos.com.ar/', 78, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(10, '2962', '491228', 's/d', 'http://www.santacruz.gob.ar', 78, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(11, '11', '55290375', 'rabdenur@gmail.com', 'http://www.santacruz.gob.ar', 86, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(12, '3843', '402187', 'sixtomadiaz@hotmail.com', 'https://www.municipalidaddequimili.gob.ar/', 86, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(13, '11', '55290375', 'rabdenur@gmail.com', 'cineatlasweb.com.ar', 90, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(14, '3482', '598468', 'gscarel@estudioscarel.com.ar', 'http://www.cinelunadeavellaneda.com', 82, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(15, '3496', '420947', 'martin739@gmail.com', 'http://www.e-max.com.ar', 82, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(16, '3562', '405750', 'admin@lastipasboulevard.com.ar', 'www.lastipasrafaela.com.ar/', 82, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(17, '342', '4573735', 'cencultu@yahoo.com.ar', 'https://es-la.facebook.com/ccpacourondo/', 82, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(18, '3493', '454542', 'infocet@atilra.org.ar', 'https://www.ate.org/listado_agenda/seccion/3/zona/26/cine-auditorio-ate.html', 82, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(19, '351', '4864350', 'pcavallero@tadicor.com', 'http://www.tadicor.com/Cines.php', 50, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(20, '351', '4864350', 'pcavallero@tadicor.com', 'http:/www.tadicor.com/seccióncines', 50, '2022-03-22 10:32:03', '2022-03-22 10:32:03'),
(58, '358', '4676836', 'gamolayoli@hotmail.com', 'http://www.cba.gov.ar/', 34, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(59, '3515', '261500', 'cluduena@grupodinosaurio.com', 'http://www.cinesdinomall.com.ar', 34, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(60, '3543', '446300', 'gerente.cordoba@cinesunstar.com', 'http://www.cinesunstar.com/', 34, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(61, 's/d', '49535405', 'gabriel.guralnik@gmail.com', 'http://www.rojas.uba.ar/', 32, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(62, 's/d', '43135050', 'info@atlascines.com.ar', 'cineatlasweb.com.ar', 32, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(63, 's/d', '48034062', 'fwhite@aamnba.org.ar', 'https://www.facebook.com/pg/amigosdelbellasartes/about/?ref=page_internal', 32, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(64, 's/d', '46741300', 'paula@granrivadavia.com.ar', 'http://www.granrivadavia.com.ar/inicio/', 32, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(65, 's/d', '57771000', 'rmazzutti@hoyts.com.ar', 'https://www.cinemarkhoyts.com.ar/', 32, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(66, 's/d', '43836432', 'fernandomadedo@cinain.gob.ar', 'http://cinain.gob.ar/', 32, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(67, 's/d', '40196019', 'mlopez@cinemadevoto.com.ar', 'https://cinemadevoto.com.ar/', 32, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(68, 's/d', '55555358', 'dpetchersky@yahoo.com.ar', 'https://www.ccborges.org.ar/', 32, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(69, '11', '65992021', 'maquintero78@gmail.com', 'https://es-la.facebook.com/ZarateCineTeatroColiseo/', 36, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(70, '221', '4248733', 'cineylocacioneslaplata@gmail.com', 'http://www.cultura.laplata.gov.ar', 36, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(71, '11', '52180400', 'gzamora@cinemacenter.com.ar', 'http://www.cinemacenter.com.ar/', 36, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(72, '2474', '551096', 'j.burgos.ceres@hotmail.com', 'http://saltoyvos.com/tag/cine_cervantes/', 36, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(73, 's/d', '51282828', 'village.avellaneda@village.com.ar', 'http://facebook.com/cinecervantes3d', 36, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(74, '11', '37608002', 'edulabce@hotmail.com', 'www.unica-cartelera.com.ar/teatros/259-sala-carlos-carella', 36, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(75, 's/d', '42430381', 'lomasteatroespanol@gmail.com', 'https://www.facebook.com/cineteatroespanol/', 36, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(76, 's/d', '51688888', 'village.merlo@village.com.ar', 'https://www.villagecines.com/', 36, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(77, 's/d', '47579861', 'inoya@tresdefebrero.gov.ar', 'https://www.tresdefebrero.gov.ar/noticias/cultura-y-educacin/reinauguracion-del-cine-teatro-ocean/', 45, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(78, '2983', '481740', 'sespagch@hotmail.com', 'https://www.facebook.com/pg/ZarateCineTeatroColiseo/about/?ref=page_internal', 45, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(79, '2314', '480184', 'analia.carbajo@live.com', 's/d', 45, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(80, 's/d', '47971162', 'york.cultura@vicentelopez.gov.ar', 'https://www.vicentelopez.gov.ar/modernizacion/punto-de-interes/3-centro-cultural-munro', 45, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(81, 's/d', 's/d', 's/d', 'https://www.facebook.com/pages/Centro-Cultural-Leopoldo-Marechal-Hurlingham/297621827101262', 45, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(82, '220', '4776523', 'cine@marcospaz.gov.ar', 'https://www.facebook.com/pg/cinemarcospaz/about/?ref=page_internal', 45, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(83, 's/d', '47675752', 'soteras_jh_am@hotmail.com', 'https://www.facebook.com/pg/CineTeatroSalaJoseHernandez/about/?ref=page_internal', 50, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(84, 's/d', '47518647', 'produccion@cineteatrohelios.com.ar', 'https://www.cinesargentinos.com.ar/horarios/c:733:cine-teatro-helios/', 50, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(85, '221', '6239105', 'proyeccionesterrestres@gmail.com', 'https://www.facebook.com/pg/Clases-de-Teatro-La-Plata-124851047580248/about/?ref=page_internal', 50, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(86, '2266', '421703', 'complejoparis1@gmail.com', 'https://web.ultracine.com/nueva-sala-cine-paris-de-balcarce/', 50, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(87, '2353', '460583', 'mutualafc@yahoo.com.ar', 'https://www.facebook.com/pages/Cine-Teatro-Roma-General-Arenales/286484061815736', 50, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(88, '2257', '460050', 's/d', 'https://www.facebook.com/pg/Cine-Teatro-Arenas-3D-San-Bernardo-309783435805259/about/?ref=page_internal', 50, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(89, '2284', '417751', 'complejoparis1@gmail.com', 'http://www.cineparis.com.ar', 50, '2022-03-22 10:58:50', '2022-03-22 10:58:50'),
(90, '2296', '454949', 'isaiasalgañaraz@gmail.com', 'https://www.facebook.com/pages/category/Government-Organization/Direcci%C3%B3n-de-Cultura-Ayacucho-1677928362483344/', 50, '2022-03-22 10:58:50', '2022-03-22 10:58:50');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_contacto_espacioCultural` (`id_esp_cultural`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD CONSTRAINT `FK_contacto_espacioCultural` FOREIGN KEY (`id_esp_cultural`) REFERENCES `cultura` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
